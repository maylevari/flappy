from bird import Bird
from ball import Ball
import random
import turtle


class FlappyGame:
    '''
    this class is the game class with the following properties:
    1. initializing game parametrs - bird size, bird color and ball color
    2. printing current score
    3. controlling the movement of the bird
    4. creating & controlling the movement of the balls
    '''

    def __init__(self, bird_size: int, bird_color: str, balls_color: str):
        self._bird = Bird(bird_color, bird_size, 0, 0)
        self._balls = []
        self._score = 0
        self._balls_size = random.randint(10, 30)
        self._balls_color = balls_color

    def print_score(self):
        "game score."
        print("you're end score is: ", self._score, " points!")

    def tap(self, x, y):
        "Move bird up in response keypad tap."
        self._bird.vector.move(x, y)

    def draw(self, alive):
        "Draw screen objects."
        turtle.clear()
        turtle.goto(self._bird.vector.x, self._bird.vector.y)
        if not alive:
            self._bird.color = 'red'
        turtle.dot(self._bird.size, self._bird.color)

        for ball in self._balls:
            turtle.goto(ball.vector.x, ball.vector.y)
            turtle.dot(ball.size, ball.color)
        turtle.update()

    def drop_bird(self):
        "Droping down the bird."
        self._bird.vector.y -= 5

    def move_balls(self):
        "moving balls to the left."
        for ball in self._balls:
            ball.vector.x -= ball.velocity

    def create_ball(self):
        "creating balls by probability."
        if random.randrange(10) == 0:
            y = random.randrange(-199, 199)
            ball = Ball(self._balls_color, 199, y)
            self._balls.append(ball)

    def check_passed_balls_and_update_score(self):
        "updating the balls that inside the board & the score according to passed balls"
        i = 0
        for ball in self._balls:
            if not ball.inside():
                self._balls.pop(i)
                self._score += 1
            i += 1

    def check_if_bird_is_alive(self):
        "Checks if the bird is still within the boundaries of the screen and has not hit any ball."
        if not self._bird.inside():
            self.draw(False)
            return False
        for ball in self._balls:
            if abs(ball.vector.x - self._bird.vector.x) < 13 and abs(ball.vector.y - self._bird.vector.y) < 13:
                self.draw(False)
                return False
        return True

    def bird_movment(self):
        # allows to move the bird by the keypad in all direction (up, down, right & left)
        turtle.onkey(lambda: self.tap(0, 30),
                     'Up')
        turtle.onkey(lambda: self.tap(0, -30), 'Down')
        turtle.onkey(lambda: self.tap(20, 0), 'Right')
        turtle.onkey(lambda: self.tap(-20, 0), 'Left')

    def start(self):
        "updating objects positions recursively."
        self.bird_movment()
        self.drop_bird()
        self.move_balls()
        self.create_ball()
        self.check_passed_balls_and_update_score()
        if self.check_if_bird_is_alive():
            self.draw(True)
            turtle.ontimer(self.start, 50)
        else:
            self.print_score()
            return
