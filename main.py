from flappy_game import FlappyGame
import turtle


def main():
    turtle.setup(420, 420, 370, 0)
    turtle.hideturtle()
    turtle.up()
    turtle.tracer(False)
    turtle.listen()
    game = FlappyGame(10, 'Green', 'Black')
    game.start()
    turtle.done()


if __name__ == '__main__':
    main()