from shape import Shape


class Bird(Shape):
    '''this class represent a bird in the game'''

    def __init__(self, color: str, size: int, x: int, y: int):
        super().__init__(color, size, x, y)

    def inside(self):
        "Return True if point on screen."
        return -200 < self.vector.x < 200 and -200 < self.vector.y < 200
