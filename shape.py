from abc import ABC, abstractmethod
from vector import Vector


class Shape(ABC):
    '''this class represent any dot on the board game'''

    def __init__(self, color: str, size: int, x: int, y: int):
        self._vector = Vector(x, y)
        self._color = color
        self._size = size

    @property
    def vector(self) -> Vector:
        return self._vector

    @property
    def color(self) -> str:
        # color of dot.
        return self._color

    @color.setter
    def color(self, color: str):
        # updating dot color
        self._color = color

    @property
    def size(self) -> int:
        # size of dot.
        return self._size

    @size.setter
    def size(self, size: int):
        # updating dot size
        self._size = size

    @abstractmethod
    def inside(self): pass
