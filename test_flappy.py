from vector import Vector
from bird import Bird
from ball import Ball


def test_vector_move():
    vector = Vector(1, 2)
    vector.move(2, 2)
    assert vector.x == 3
    assert vector.y == 4


def test_bird_inside_yes():
    bird = Bird("Green", 10, 1, 2)
    assert bird.inside() == True


def test_bird_inside_no():
    bird = Bird("Green", 10, 201, 2)
    assert bird.inside() == False


def test_ball_inside_yes():
    ball = Ball("Black", 50, 50)
    assert ball.inside() == True


def test_ball_inside_no():
    ball = Ball("Black", 2, 201)
    assert ball.inside() == False
