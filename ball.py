from shape import Shape
import random


class Ball(Shape):
    '''this class represent a ball in the game'''

    def __init__(self, color: str, x: int, y: int):
        super().__init__(color, random.randint(10, 30), x, y)
        self._velocity = random.randint(1, 10)

    @property
    def velocity(self) -> int:
        # Y-axis component of vector.
        return self._velocity

    def inside(self):
        "Return True if point on screen."
        return -200 < self.vector.x < 200 and -200 < self.vector.y < 200
