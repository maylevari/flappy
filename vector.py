class Vector:
    '''this class represent a vector on the board game'''

    def __init__(self, x: int, y: int):
        self._x = x
        self._y = y

    @property
    def x(self) -> int:
        # Y-axis component of vector.
        return self._x

    @x.setter
    def x(self, value: int):
        # updating X-axis position
        self._x = value

    @property
    def y(self) -> int:
        # Y-axis component of vector.
        return self._y

    @y.setter
    def y(self, value: int):
        # updating Y-axis position
        self._y = value

    def move(self, x_to_move, y_to_move):
        # adding movement to vector by updating X-axis and Y-axis positions
        self.x += x_to_move
        self.y += y_to_move
